console.log("hello world!");

// [SECTION] WHILE LOOP
	// A while loop takes in an expression/condition.
	// Expression/s is/are any unit that can be evaluated to a value.
	// if the condition evaluates to be tru the statements inside the code block will be executed.
	//  ALoop will iterate a certain number of times until an expression/condition evaluates as false.
// Iteration is the term given to the repitition of statement.

/*
	syntax:

	while(expression/condition){
		statements;
		increment/decrement;
	}
*/

let count = 10;
	
	while(count!=0){
		
		console.log("The current value of count is "+count);
		count--;
		// Decreases the value of count of 1 after every iteration to stop the loop when it reaches the 0.
		// Loops occupy a significant amount of memory space in our devices.
		// Reminder: Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop.
	}


// [SECTION] DO-WHILE LOOP
	// A do- while loop works like a while loop but unlike while loops, do-while loop guarantee that the code will be executed atleast once.

	/*
		syntax:

		do{
			statement;
			increment/decrement;
		}
		while(expression/condition);
	*/ 

	// Number() = converts the input of the user from str to number.
	// let number = Number(prompt("Give a number: "));
	// do{
	// 	console.log("Do-while: "+number);
	// 	number++;
	// }
	// while(number<=10);

// [SECTION] FOR LOOP
	/*
		- A for-loo is more flexible that while and do-while.
		It consists of 3 parts:
		1. The "Initialization" value taht will track the progression of the loop.
		2. The "expression/condition" that will be evaluated which will be determine whether the loop will run one more time.
		3. The "finalExpression" indicates how the loop will advance.

	*/
	/*
		Syntax:
		for(initialization; expression/condition; finalExpression){
			statement/s;
		}
	
	*/
		// Business Logic
			// 1.We will create a loop taht will start from zero and end at 20.
			// 2. Every iteration of the loop, the value of count will be checked if it is equal or than 20.
			// 3.If the value of caount is less than or equal to 20 the statement inside teh loop will run 
			// 4. The value of count will be incremented by one by each iteration.

	for(let count = 0; count<=20; count++){
		console.log("The current value of count is "+count);
	}

	let myString = "Christopher";
		//characters in string may be counted using the . length property.
	// Stringa are special compare to other data types taht it has access to functions and other pieces of information

		console.log(myString.length); 

	/*
		Create a loop that will print out the letters of our name individually.
	*/
	let myName = "Danbell";
	for(let i=0; i < myName.length; i++){
		// console.log(myName[i]);
		myName.toLowerCase();
		if(myName[i]=== "a" || myName[i]=== "e" || myName[i]=== "i" || myName[i]=== "o" || myName[i]=== "u" ){
			myName[i]="";
		}
		// else{
		// 	console.log(myName[i]);
		// }
	}
	console.log(myName);